import '../models/data_layer.dart';
import 'package:flutter/material.dart';
import '../provider/plan_provider.dart';
import '../provider/plan_provider.dart';

class PlanDetailScreen extends StatefulWidget{
  final Plan plan;
  const PlanDetailScreen({Key? key,required this.plan}): super(key: key);
  @override
  State createState() => _PlanDetailState();
}

class _PlanDetailState extends State<PlanDetailScreen>{
  late ScrollController scrollController;
  Plan get plan => widget.plan;
  @override
  void initState(){
    super.initState();
    scrollController = ScrollController()
    ..addListener((){
      FocusScope.of(context).requestFocus(FocusNode());
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Plan Detail')),
      body: Column(children: <Widget>[
        Expanded(child: buildList()),
        SafeArea(child: Text(plan.completenessMessage))
    ],),
    floatingActionButton: buildAddTaskBtn()
    );
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  Widget buildAddTaskBtn() {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        final controller = PlanProvider.of(context);
        controller.createNewTask(plan);
        setState(() {});
      },
    );
  }

  Widget buildList() {
    //final plan = PlanProvider.of(context);
    return ListView.builder(
      controller: scrollController,
      itemCount: plan.tasks.length,
      itemBuilder: (context, index) => buildTaskTile(plan.tasks[index]),
    );
  }
  Widget buildTaskTile(Task task) {
    return Dismissible(
      key: ValueKey(task),
      background: Container(color: Colors.red),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        final controller = PlanProvider.of(context);
        controller.deleteTask(plan, task);
        setState(() {});
      },
      child: ListTile(
        leading: Checkbox(
            value: task.complete,
            onChanged: (selected) {
              setState(() {
                task.complete = selected as bool;
              });
            }),
        title: TextFormField(
          initialValue: task.description,
          onFieldSubmitted: (text) {
            setState(() {
              task.description = text;
            });
          },
        ),
      ),
    );
  }



}