import 'package:flutter_test/flutter_test.dart';
import '../lib/repositories/repository.dart';
import '../lib/repositories/in_memory_cache.dart';

void main(){
  Repository repo = InMemoryCache();
  test('Test InMemory Initial Repository ',(){
    expect(repo.getAll().isEmpty,true);

    Model newModel = repo.create();
    expect(newModel.id,1);
    expect(repo.getAll().isEmpty,false);

    Model? existedModel = repo.get(1);
    expect(newModel,existedModel);

    Model newModel2 = repo.create();
    expect(repo.getAll().length,2);

    existedModel = repo.get(2);
    expect(newModel2,existedModel);

    // testing task of plan
    Model newModel3 = Model(id: 3, data: {'task_name' : 'Finish all assignments'});
    expect(newModel3.id, 3);

    expect(newModel3.data.isEmpty, false);

    expect(newModel3.data['task_name'], 'Finish all assignments');

  });
}