import '../services/plan_services.dart';
import '../models/data_layer.dart';

class PlanController {
  final services = PlanServices();

  List<Plan> get plans => List.unmodifiable(services.getAllPlans());

  void addNewPlan(String name){
    if(name.isEmpty)
      return;
    name = checkDuplicates(plans.map((p)=>p.name),name);
    services.createPlan(name);
  }

  void savePlan(Plan p){
    services.savePlan(p);
  }

  void deletePlan(Plan p){
    services.deletePlan(p);
  }

  void createNewTask(Plan p,[String? desc]){
    if(desc == null || desc.isEmpty){
      desc = 'New Task';
    }
    desc = checkDuplicates(p.tasks.map((task)=>task.description),desc);
    services.addTask(p, desc);
  }

  void deleteTask(Plan p,Task t){
    services.deleteTask(p, t);
  }


  String checkDuplicates(Iterable<String> items, String text) {
    final duplicatedCount = items.where((item) => item.contains(text)).length;
    if (duplicatedCount > 0) {
      text += ' ${duplicatedCount + 1}';
    }

    return text;
  }

}