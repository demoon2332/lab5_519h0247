import '../controllers/plan_controller.dart';
import '../models/data_layer.dart';
import 'package:flutter/material.dart';
import '../controllers/plan_controller.dart';

class PlanProvider extends InheritedWidget{
  final controller = PlanController();

  PlanProvider({Key? key,required Widget child}): super(key: key,child: child);

  @override
  bool updateShouldNotify(InheritedWidget widget) => false;

  static PlanController of(BuildContext context){
    PlanProvider provider = context.dependOnInheritedWidgetOfExactType<PlanProvider>() as PlanProvider;
    return provider.controller;
  }
}