import '../repositories/in_memory_cache.dart';
import '../models/task.dart';
import '../models/plan.dart';
import '../repositories/repository.dart';

class PlanServices{
  Repository repo = InMemoryCache();

  Plan createPlan(String name){
    final model = repo.create();
    final p = Plan.fromModel(model)..name = name;
    savePlan(p);
    return p;
  }

  void savePlan(Plan p){
    repo.update(p.toModel());
  }

  void deletePlan(Plan p){
    repo.delete(p.toModel().id);
  }

  List<Plan> getAllPlans(){
    return repo.getAll()
        .map<Plan>((m) => Plan.fromModel(m))
        .toList();
  }

  void addTask(Plan plan,String description){
    // print('plan.task=${plan.tasks}');
    // print('plan.tasks.last = ${plan.tasks.last}');

    final id = plan.tasks.length+1;
    final task = Task(id: id, description: description);
    plan.tasks.add(task);
    savePlan(plan);
  }

  void deleteTask(Plan p,Task t){
    p.tasks.remove(t);
    savePlan(p);
  }
}