import 'plan_detail_screen.dart';
import 'package:flutter/material.dart';

import '../provider/plan_provider.dart';

class PlanMakerScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _PlanMakerScreenState();
  }

}

class _PlanMakerScreenState extends State<PlanMakerScreen>{
  final planController =  TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My Plans'),),
      body: Column(
        children: [
          _buildList(),
          Expanded(child: buildDetailPlan())
        ],
      ),
    );
  }

  Widget _buildList(){
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Material(
        child: TextField(
          controller: planController,
          decoration: InputDecoration(
            labelText: 'Add a new plan'
          ),
        )
    ),
    );
  }

  Widget buildDetailPlan(){
    final plans = PlanProvider.of(context).plans;
    if(plans.isEmpty){
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.note,size: 100,color: Colors.grey),
          Text('You did not have any plans yet.',
          style: Theme.of(context).textTheme.headline5,)
        ],
      );
    }
    else{
      return ListView.builder(
        itemCount: plans.length,
          itemBuilder: (context,index){
            final plan = plans[index];
            return Dismissible(
              key: ValueKey(plan),
              background: Container(color: Colors.red),
                direction: DismissDirection.endToStart,
              onDismissed: (_){
                final controller = PlanProvider.of(context);
                controller.deletePlan(plan);
                setState(() {

                });
              },
              child: ListTile(
                title: Text(plan.name),
                subtitle: Text(plan.completenessMessage),
                onTap: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (_)=> PlanDetailScreen(plan: plan)));
                },
              ),
            );
          });
    }
  }

}























