import 'package:flutter/material.dart';
import 'plan_maker_screen.dart';

class PlanApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Planner',
      home: Scaffold(
        body: PlanMakerScreen(),
      )
    );
  }

}