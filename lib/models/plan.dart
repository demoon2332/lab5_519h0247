import '../repositories/repository.dart';
import 'task.dart';

class Plan{
  int id =0;
  String name ='';
  List<Task> tasks=[];

  Plan({required this.id,this.name =''});

  Plan.fromModel(Model model){
    id = model.data['id'];
    name= model.data['name']??'';
    if(model.data['task'] != null){
      tasks = model.data['task'].map<Task>((task)=>Task.fromModel(task)).toList();
    }
  }

  Model toModel()=> Model(id: id,data: {
    'name': name,
    'tasks': tasks.map((t) => t.toModel()).toList()
  });

  int get completeCount => tasks.where((t) => t.complete).length;

  String get completenessMessage => '$completeCount out of ${tasks.length} tasks';
}
